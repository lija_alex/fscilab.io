---
title: "Hardware that respect your freedom!"
date: 2021-04-26T22:32:25+05:30
author: false
draft: true
slug: 'ryf'
---
**Criteria to be included in this list**: Hardware should be able to run a Free/Libre operating system without any proprietary blobs. Devices should also preferably have a free system firmware - like coreboot or libreboot with hardware backdoors disabled if present like the Intel ME backdoor. 

List of mobile phones:

- [Librem](https://puri.sm/products/librem-5/) 5 by Purism 

Laptops:
    
- Librem laptops by Purism https://puri.sm/

- LibreTech laptops from [Mostly Harmless](https://libretech.shop/)

- Technoethical ([T400](https://tehnoetic.com/TET-T400) , [T400s](https://tehnoetic.com/TET-T400s) , [T500](https://tehnoetic.com/TET-T500), [X200](https://tehnoetic.com/TET-X200) , [X200s](https://tehnoetic.com/TET-X200s))

- [Vikings X200](https://store.vikings.net/libre-friendly-hardware/x200-ryf-certfied)

- [Taurinus X200](https://shop.libiquity.com/product/taurinus-x200)

Honorable Mentions:

- [Pinephone](https://www.pine64.org/pinephone/) - Can run GNU/Linux distro.
