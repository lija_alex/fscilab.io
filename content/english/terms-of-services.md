---
title: "Terms of Services"
date: 2021-07-17T01:31:36+05:30
author: false
draft: false
slug: tos
aliases:
    - /terms-of-services
---

The following terms apply to all services we maintain, each service may have additional terms specific to that service.

As an Intermediary, we as a provider and you as a user have to follow the following conditions as well as specified by The Information Technology (Intermediary Guidelines and Digital Media Ethics Code) Rule, 2021:

The rules and regulations, privacy policy or user agreement of the intermediary shall inform the user of its computer resource not to host, display, upload, modify, publish, transmit, store, update or share any information that,—

(i) belongs to another person and to which the user does not have any right;

(ii) is defamatory, obscene, pornographic, pedophilia, invasive of another‘s privacy, including bodily privacy, insulting or harassing on the basis of gender, libellous, racially or ethnically objectionable, relating or encouraging money laundering or gambling, or otherwise inconsistent with or contrary to the laws in force;

(iii) is harmful to child;

(iv) infringes any patent, trademark, copyright or other proprietary rights;

(v) violates any law for the time being in force;

(vi) deceives or misleads the addressee about the origin of the message or knowingly and intentionally communicates any information which is patently false or misleading in nature but may reasonably be perceived as a fact;

(vii) impersonates another person;

(viii) threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with foreign States, or public order, or causes incitement to the commission of any cognisable offence or prevents investigation of any offence or is insulting other nation;

(ix) contains software virus or any other computer code, file or program designed to interrupt, destroy or limit the functionality of any computer resource;

(x) is patently false and untrue, and is written or published in any form, with the intent to mislead or harass a person, entity or agency for financial gain or to cause any injury to any person;

**You can read the full notification [here](http://egazette.nic.in/WriteReadData/2021/225464.pdf) or it's archived version on [archive.org](https://web.archive.org/web/20210411161305/http://egazette.nic.in/WriteReadData/2021/225464.pdf).**

**We don't fully agree with the notification and have challenged it Kerala High Court with help from SFLC India. You can read more about it [here](https://sflc.in/praveen-arimbrathodiyil-vs-union-india-sflcin-assists-challenging-part-ii-intermediary-rules-2021).**

## Questions?

Feel free to reach us via email - admin AT fsci.in for any questions or reporting abuse of services.