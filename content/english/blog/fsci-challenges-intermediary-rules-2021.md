---
title: "FSCI challenges new Intermediary Rules, 2021, in the High Court of Kerala"
date: 2021-04-15T00:33:03+05:30
author: "false"
tags: ["privacy"]
draft: false
discussionlink: https://codema.in/d/vHye2A6p/how-do-we-respond-to-the-information-technology-intermediary-guidelines-and-digital-media-ethics-code-rules-2021-?p=7
---

FSCI has challenged Part II of the Intermediary Rules, 2021 with the help of SFLC India to declare these rules as unconstitutional. The analysis by SFLC India and IFF show us how far-reaching the impact these rules have on our right to privacy and freedom of speech and expression. As a community providing communication services to the general public, FSCI is directly affected by these rules. Our ability to provide privacy-respecting services to the public is affected. We are also forced to censor people's free speech or be liable for the user generated content we host. The petition has been admitted by Kerala High Court and is pending for hearing.

Summary of the petition:

A copy of the petition is available at https://sflc.in/praveen-arimbrathodiyil-vs-union-india-sflcin-assists-challenging-part-ii-intermediary-rules-2021.

1. The Intermediary Rules, 2021 place unreasonable restrictions on users in expressing themselves online, thus impacting their right to free speech and expression and fundamental right to privacy.

1. The terms used in the Intermediary Rules, 2021 s are vague and ambiguous.
1. The  Intermediary Rules, 2021 seek to undermine end-to-end encryption which is a subset of fundamental right to privacy as enshrined in Justice K.S. Puttaswamy vs. Union of India (2017).
1. The Intermediary Rules, 2021 draw no intelligible differentia between not-for-profit FOSS communities and for-profit proprietary companies.
1. The Intermediary Rules, 2021 place a huge compliance burden on FOSS entities or services thus, impacting their right to trade and business.
1. That there was a lack of stakeholder consultation in contravention of the Government of India’s Pre-Legislative Consultation Policy.
1. The rules are in contravention to Shreya Singhal V. Union of India (2015).
1. The Intermediary Rules, 2021 prescribe technology-based solutions such as automated tools which would bring in inherent societal biases and prejudices leading to more problems than it intends to solve.
1. The Intermediary Rules, 2021 are a delegated legislation and are ultra vires as they are inconsistent with the Information technology Act, 2000 i.e. the parent legislation.
1. The Intermediary Rules, 2021 also place an adjudicatory role on intermediaries.

We are highly grateful for SFLC India who helped us in challenging these new IT rules so that we can fight for our rights in the digital space.

## What is Intermediary Guidelines and Digital Media Ethics Code Rules, 2021?

Indian government has [announced new rules](http://egazette.nic.in/WriteReadData/2021/225464.pdf) under the Information Technology Act for the regulation of social media, digital publishing media and OTT Platforms which will significantly increase the level of surveillance and censorship in the country. Social media intermediaries will either be forced to remove end-to-end encryption to comply with the rules or banned by the government for violating the rules. We have already seen how TikTok and many other apps were banned [citing that these apps were violating](https://www.pib.gov.in/PressReleasePage.aspx?PRID=1650669) section 69A of the Information Technology Act showing that the government can exercise the power to ban any digital services at any time.

Before these rules were announced, we used to say using Free Software powered services hosted by people we trust was sufficient for ensuring privacy, but now that is not sufficient as they will also be forced to remove end to end encryption or get banned.

Further the Petition by Foundation for Independent Journalism mentioned "The [rules seek to regulate](https://www.bloombergquint.com/law-and-policy/intermediary-and-social-media-guidelines-challenged-in-courts-by-digital-news-portals) digital news media by imposing a ‘Code of Ethics’, with all manner of stipulations as to ‘half-truths’, ‘good taste’, ‘decency’ etc., and vest the power of interference ultimately with the central government as the chief regulator, at the highest of three tiers." The rules threaten the freedom of speech, privacy, security and freedom of all citizens of India.

These rules have far-reaching consequences as all the government institutions, armed forces and entire country need to compromise their privacy and security for online communications by breaking end-to-end encryption.

## Right to Privacy is a fundamental right, it is intrinsic to right to life

In 2017, Supreme court ruled that the [Right to Privacy is a fundamental right](https://timesofindia.indiatimes.com/india/right-to-privacy-is-a-fundamental-right-supreme-court/articleshow/60203394.cms) for Indian citizens under the Constitution of India. Thus no legislation passed by the government can unduly violate it.

End-to-end encryption is an essential component for digital privacy. Compliance with these rules would require intermediaries to break end-to-end encryption essentially violating every citizen's right to privacy.

[The LiveLaw article](http://livelaw.in/columns/the-puttaswamy-test-right-to-privacy-article-21-171181) introduces encryption very well which we would like to quote below:
"Encryption technology has popularly been used for varied purposes including secure communication. This allows for individuals to communicate amongst each other without anyone else being able to monitor the content of such conversations.  This, thus, keeps such conversations even outside the purview of Law  Enforcement Agencies ('LEAs').  This preserves the privacy of the users and allows them to safely  communicate without any threats of surveillance. However, this aspect of  encryption technologies has also led to States, including India, seeking the  introduction of 'backdoors' to this technology, particularly citing concerns of  misinformation, child pornography, and national security."

Only the users involved in a communication can read the end to end encrypted messages.

## Dangers of massive surveillance

Eliminating end-to-end encryption in digital life is analogous to eliminating the need for a search warrant by police. The very reason behind the existence of warrants is the thesis that a person is considered innocent till proven guilty. The new IT rules reverse this fundamental principle and treat everyone as criminals suspects until proven innocent. In turn this has consequences for journalists and whistle blowers since the new IT rules grant the government the power to demand identity of sources of journalists or whistle blowers and identify the dissent before it reaches public. People will be afraid of expressing dissenting views against the government. Whistleblowers will be afraid to get caught because of lack of anonymity effectively leading people to self-censor.

Statistics show that [lack of privacy leads to a population who is afraid to ask questions or educate themselves](https://theintercept.com/2016/04/28/new-study-shows-mass-surveillance-breeds-meekness-fear-and-self-censorship/), even if the issues are important and the motives are pure.

We'd like quote some points from this excellent resource about effects of massive surveillance at [Social Cooling](https://www.socialcooling.com/).

"If you feel you are being watched, you change your behavior." This could limit your desire to take risks or exercise free speech. Over the long term these 'chilling effects' could 'cool down' society.
Rating systems can create unwanted incentives, and increase pressure to conform to a bureaucratic average. When doctors in New York were given scores, [this had unexpected results](https://science.slashdot.org/story/15/07/22/1752209/giving-doctors-grades-has-backfired).
Doctors that tried to help advanced cancer patients had a higher mortality rate, which translated into a lower score. Doctors that didn't try to help were rewarded with high scores, even though their patients died prematurely.

People are starting to realize that this 'digital reputation' could limit their opportunities:

- [You may not get that dream job if your data suggests you're not a very positive person](https://www.theguardian.com/science/2016/sep/01/how-algorithms-rule-our-working-lives).

- [If you are a woman you may see fewer ads for high paying jobs](https://www.theguardian.com/technology/2015/jul/08/women-less-likely-ads-high-paid-jobs-google-study).

- [If you have "bad friends" on social media you might pay more for your loan](https://trustingsocial.com/).

- [Tinder's algorithms might not show you attractive people if you are not desirable yourself](https://www.fastcompany.com/3054871/whats-your-tinder-score-inside-the-apps-internal-ranking-system).

- [Cambridge Analytica created psychological profiles on all Americans to try and dissuade people from voting](http://adage.com/article/media/broke-facebook-cambridge-analytica-whistleblower/312791/).

- [If you return goods to the store often this will be used against you](https://apprissretail.com/).

- [What you post on social media may influence your odds of getting a tax audit](https://news.slashdot.org/story/17/08/29/2333209/the-irs-decides-who-to-audit-by-data-mining-social-media).

- [Your health insurer may collect intimate data about your lifestyle, race and more](https://www.propublica.org/article/health-insurers-are-vacuuming-up-details-about-you-and-it-could-raise-your-rates).

## Don't burn your house to scare away a mouse!

The rules might make the encrypted apps illegal and it is naive to expect criminals to not use illegal apps and as a result all the law-abiding citizens would be put under more surveillance while criminals would enjoy more privacy and security--an irony. This reminds us of of the proverb 'Don't burn your house to scare away a mouse'.

## Can we communicate without an intermediary?

Peer-to-peer apps like Briar, GNU Jami, Tox etc. which transmits messages directly from one person's device to another person's device without using any intermediary. For this method both parties have to be online at the same time.

## What are the government's intentions?

The government has argued against the right to privacy of citizens in the court https://www.hindustantimes.com/india/citizens-do-not-have-fundamental-right-to-privacy-centre-tells-sc/story-ykRepEFYCvWteceqLNuz9O.html.

## Further Reading:

- [FSCI's petition to challenge the IT rules](https://sflc.in/praveen-arimbrathodiyil-vs-union-india-sflcin-assists-challenging-part-ii-intermediary-rules-2021)

- [Software Freedom Law Centre analysis on new IT rules](https://sflc.in/analysis-information-technology-intermediary-guidelines-and-digital-media-ethics-code-rules-2021)

- [Internet Freedom Foundation's report](https://internetfreedom.in/constitutional-questions-against-unconstitutional-rules/)

- [A wolf in watchdog’s clothing: On government’s move to regulate digital media](https://www.thehindu.com/opinion/editorial/a-wolf-in-watchdogs-clothing-the-hindu-editorial-on-indian-governments-move-to-regulate-digital-media-platforms/article33956670.ece)

- [How Much Surveillance Can Democracy Withstand?](https://www.gnu.org/philosophy/surveillance-vs-democracy.en.html)

- [Dear MEITY, withdraw the new IT Rules!](https://internetfreedom.in/withdraw-the-it-rules/)

- [Social Cooling](https://www.socialcooling.com/)

- [Does The Traceability Requirement Meet The Puttaswamy Test?](http://livelaw.in/columns/the-puttaswamy-test-right-to-privacy-article-21-171181)