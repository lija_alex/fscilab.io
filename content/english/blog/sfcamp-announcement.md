---
title: "Announcing Software Freedom Camp 2021 (Diversity Edition)"
date: 2021-07-30T01:31:49+05:30
author: "false"
tags: ["camp","announcement"]
draft: false
---

Software Freedom Camp (Diversity Edition) 2021 is an online mentorship programme focusing on diversity organized by [Free Software Community of India](https://fsci.in) and inspired by [Free Software Camp 2020](https://camp.fsf.org.in) and [Outreachy](https://www.outreachy.org/). You can learn more about the camp by visiting the [camp website](https://camp.fsci.in). 

We have now opened our mailing list for announcements, Matrix/XMPP/IRC powered chat room and Mastodon/Pleroma/Fediverse accounts to receive updates about this year's Software Freedom Camp Diversity edition. Feel free to join/follow us on any of these mediums for announcement regarding opening of registration and other updates regarding the camp.

If you want to join as a learner or a mentor, please check [this link](https://camp.fsci.in/#subscribe).

We are also looking for more people to join our organizing team. If you are interested, write an email to camp at fsci.in with a short introduction about yourself.

Looking forward to your participation in the camp.

Codema Link: https://codema.in/d/rkfOi48N/a-blog-post-on-announcing-software-freedom-camp-2021-diversity-edition-
