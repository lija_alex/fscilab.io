---
title: "Setup Private Dns With Open Nic Servers"
date: 2020-08-03T20:42:09Z
author: "Pirate Praveen"
author_about: "https://poddery.com/people/45fa8bea21b8a0f5"
tags: ["private-dns", "dns", "open-nic", "article"]
draft: false
discussionlink: https://codema.in/d/ZL6qpsAj/setup-private-dns-with-open-nic-servers
---

Recently many ISPs blocked DuckDuckGo.com at DNS level making the privacy focused search engine unavailable for many Internet users in India. DNS or Domain Name Service is like an address book for the internet used by all internet connected apps like your Web Browser or Instant Messaging app. A name server (a server running a DNS service) returns the Internet Protocol (IP) address for any registered domain names to any application requesting it. For example, duckduckgo.com has address 40.81.94.43.

So when your ISP's name server blocks a domain like DuckDuckGo.com, your apps will not get the correct IP address when they request for it. To work around for this, we can change the name server from ISP provided value to another independent name server that does not censor any domains. On Desktop/Laptop or on your routers, this is easy to change, you just have to give IP address of the name server in network settings. But on Android this is not straightforward, you may need root access and install specialized apps for this.

But from Android version 9, there is an option to setup Private DNS which support DNS over TLS (DoT). There are many tutorials on the internet to set this up using popular DNS services like Google DNS, Cloudflare DNS, Quad9 etc. But this article focus on using DNS service provided by [Open NIC project](https://www.opennic.org/).

Open NIC project has many name servers run by volunteers from across the glob. Two name servers that support Private DNS/DoT are: ns1-dot.iriseden.fr, ns2-dot.iriseden.fr. You can find more servers with DoT support at [OpenNIC Public Servers list](https://servers.opennic.org/).

You can also see a list of DNS services with DoT support listed [here](https://www.privacytools.io/providers/dns/)

### Setting up Private DNS in Android
- Choose "Network & Internet" from Settings.
 
 <div style="display:flex">
 <img src="../images/settings.png" height="500px" style="margin:auto">
 </div>

- Choose "Advanced", then " Private DNS"

 <div style="display:flex; flex-wrap: wrap">
 <img src="../images/settings-network.png" height="500px" style="margin: 30px auto">

 <img src="../images/settings-network-advanced.png" height="500px" style="margin:auto">
 </div>

- Select "Private DNS server hostname" and enter "ns1-dot.iriseden.fr" or another Service with DoT support and click "Save". 

 <div style="display:flex">
 <img src="../images/adding-dns.png" height="500px" style="margin:auto">
 </div>
 
- Now you should see the hostname you entered below Private DNS.
 
 <div style="display:flex">
 <img src="../images/dns-active.png" height="500px" style="margin:auto">
 </div>
