---
title: "Reclaim Freedom & Privacy - An Open Letter to Organizations"
date: 2020-05-24T16:40:50+05:30
author: "false"
tags: ["privacy"]
draft: false
---
As a follow up of the [letter](https://fsci.in/statements/arogya-sethu-protection-of-privacy-autonomy-and-dignity-of-workers-during-covid19-outbreak/) by Internet Freedom Foundation (IFF) on Arogya Setu and protection of privcay, autonomy and dignity of workers during COVID-19 outbreak, IFF organized a conference call on May 21st. Praveen Arimbrathodiyil participated in the call representing FSCI and based on the discussions he came up with an open letter to encourage organizations to reclaim freedom and privacy using Free Software. You can read the full letter below:

> Free Software Community of India is a collective of Free Software (sometimes also called as Open Source Software) users, advocates and developers. You can learn more about us and participate in our discussions at https://fsci.in
>
> In our day to day use of technology - be it smartphones for personal use or laptop for work, we use a lot of Software directly ( like Word processing, Image editing, Web Browsing) and indirectly (someone else runs the software in their computers and let you access it via their apps or your browser, for example WhatsApp, Google Drive or Instagram). Many popular software are proprietary and we depend on the companies who sell these software for features and support. In case of someone else running the software for you, in addition to depending on software, you additionally have to share your data with the service provider.
>
> Many times, unknowingly and because of lack of awareness, we get seduced into using software that compels us to share information about us as well our work in return for the convenience of using a piece of software or service. If we can become aware of such trade-offs, then we can also make other choices that can protect our privacy.
>
> Many of us who value our freedom and privacy create software that gives you the 4 freedoms - to use, study, modify and share (We call it Free Software due to these freedoms provided to the user). We also run services built using Free Software for you to replace many of the proprietary services you use every day.
>
> You might be already familiar with some of the Free Software like Mozilla Firefox, VLC Media Player, Libre Office and may even be using them already. But you may not be aware of services provided using Free Software running on a server, for ex:- services like chat platform, email, collaborative editing platform etc, run at various community sustained service providers like https://poddery.com, https://disroot.org, https://cryptpad.fr. In short, you can continue to enjoy all of the software and services you use without sacrificing your Freedom or Privacy in return.
>
> We can help you switch to these services or even help you set these up in computers/servers you have complete control over. For example, there are multiple Free Software replacements for video conferencing to replace Zoom and Internet Freedom Foundation is using a Free Software called Big Blue Button to host video conferences. FSCI also has our own video conferencing platform at https://meet.fsci.in open to the public and powered by another Free Software called Jitsi (currently under testing for 2 months for evaluation of its usefulness and a decision will be made if we want to continue running it after 2 months).
>
> **Freedoms, Privacy and surveillance:**
>
> Surveillance programmes like Aadhaar and Aarogya Sethu by governments and surveillance by private companies like Google, Facebook etc needs different responses. To respond to surveillance by governments, we need legal and political response. Legal response to protect the right to privacy guaranteed by the constitution and long term political work to stop governments from taking away our right to privacy when the judiciary fail to stand up for our rights.
>
> To respond to surveillance by private companies, we need individual and collective actions to recognise the privacy vs convenience trade-offs we make as a first step. Next step then is building and operating services that can replace the popular day to day communication and collaboration services. This requires money and effort, at least from some people, who recognise and value the role privacy plays in ensuring Freedom of thought and expression in a Democratic society.
>
> Organizations that can run this services on their own should do it for their use and ideally open the usage of services to other organizations and public. Communities like FSCI and Disroot, who offer these services to public needs support from everyone who values privacy to be sustainable.
>
> You can contact us via email - admin at fsci.in, if you want any help in switching to Free Software replacements. You can also join our discussion forum at https://codema.in/free-software-community-of-india-fsci/ and post your questions about Free Software and taking back your privacy with Free Software. The discussion forum we use, codema.in, is powered by a free software called loomio and it can be an excellent tool for many non-profit organizations to adopt for organizational discussions and decision making.

*See the original discussion thread on [codema.in](https://codema.in/d/UCOP3ieV/urgent-endorsement-request) for more details.*
